﻿using System;
using System.Collections.Generic;

namespace Serializer
{
    [Serializable]
    class Model
    {
        private List<Point> points;
        private List<Vector> vectors;
        private List<Normale> normales;
        private List<Plane> planes;

        public string Type { get; set; }

        public string Use_mtl { get; set; }

        public string mtl { get; set; }

        public Model(string mtl, string type, List<Point> points, List<Vector> vectors, List<Normale> normales, 
            string usemtl, List<Plane> planes)
        {
            this.mtl = mtl;
            this.Type = type;
            this.Points = points;
            this.Vectors = vectors;
            this.Normales = normales;
            this.Use_mtl = usemtl;
            this.Planes = planes;
        }

        public List<Point> Points
        {
            get => points;
            set
            {
                if (value != null) points = value;
                else throw new ArgumentNullException();
            }
        }

        public List<Vector> Vectors
        {
            get => vectors;
            set
            {
                if (value != null) vectors = value;
                else throw new ArgumentNullException();
            }
        }

        public List<Normale> Normales
        {
            get => normales;
            set
            {
                if (value != null) normales = value;
                else throw new ArgumentNullException();
            }
        }

        public List<Plane> Planes
        {
            get => planes;
            set
            {
                if (value != null) planes = value;
                else throw new ArgumentNullException();
            }
        }

        public override string ToString()
        {
            var result = $"mtllib {mtl}\n";
            result += $"o {Type}\n";
            points.ForEach(p => result += p.ToString() + "\n");
            vectors.ForEach(vt => result += vt.ToString() + "\n");
            normales.ForEach(vn => result += vn.ToString() + "\n");
            result += $"usemtl {Use_mtl}\n";
            planes.ForEach(pl => result += pl.ToString() + "\n");
            return result;
        }
    }
}
