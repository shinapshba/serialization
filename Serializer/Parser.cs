﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Serializer
{
    class Parser
    {
        public static Model GetModel(string objPath)
        {
            var points = new List<Point>();
            var vectors = new List<Vector>();
            var normales = new List<Normale>();
            var planes = new List<Plane>();
            var type = string.Empty;
            var mtl = string.Empty;
            var usemtl = "None";
            string[] words;
            double x, y, z;
            foreach (string line in File.ReadAllLines(objPath))
            {
                words = line.Split(' ');
                var prefix = words[0];
                switch (prefix)
                {
                    case "v":
                        x = Double.Parse(words[1].Replace(".", ","));
                        y = Double.Parse(words[2].Replace(".", ","));
                        z = Double.Parse(words[3].Replace(".", ","));
                        points.Add(new Point(x, y, z));
                        break;
                    case "vt":
                        x = Double.Parse(words[1].Replace(".", ","));
                        y = Double.Parse(words[2].Replace(".", ","));
                        vectors.Add(new Vector(x, y));
                        break;
                    case "vn":
                        x = Double.Parse(words[1].Replace(".", ","));
                        y = Double.Parse(words[2].Replace(".", ","));
                        z = Double.Parse(words[3].Replace(".", ","));
                        normales.Add(new Normale(x, y, z));
                        break;
                    case "usemtl":
                        usemtl = line.Split(' ')[1];
                        break;
                    case "f":
                        var planesStr = line.Replace("f ", "").Split(' ');
                        planes.Add(new Plane(planesStr));
                        break;
                    case "o":
                        type = words[1];
                        break;
                    case "mtllib":
                        mtl = words[1];
                        break;
                }
            }

            return new Model(mtl, type, points, vectors, normales, usemtl, planes);
        }
    }
}
