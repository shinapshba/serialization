﻿using System;
using System.IO;

namespace Serializer
{
    class Program
    {
        static void Main(string[] args)
        {
        begin:
            {
                try
                {
                    Console.Write("Command Serialize/Deserialize (s/d)? ");
                    var command = Console.ReadLine();
                    string objPath;
                    Model model;
                    switch (command)
                    {
                        case "s":
                            Console.Write("Object file path:\n> ");
                            objPath = Console.ReadLine();
                            model = Parser.GetModel(objPath);
                            var serializedPath = Stager.Serialize(model, objPath);
                            break;
                        case "d":
                            Console.Write("Byte file path:\n> ");
                            objPath = Console.ReadLine();
                            model = Stager.Deserialize(objPath);
                            var savePath = objPath.Replace("_serialized.dat", "_deserialized.obj");
                            File.WriteAllText(savePath, model.ToString());
                            break;
                        default:
                            Console.WriteLine("Enter command 's' or 'd'");
                            goto begin;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"\n{e.Message}");
                }
                Console.WriteLine("Done.");
                Console.ReadKey();
            }
        }
    }
}
