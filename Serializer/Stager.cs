﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Serializer
{
    class Stager
    {
        public static string Serialize(object obj, string filePath)
        {
            filePath = filePath.Replace(".obj", "_serialized.dat");
            var formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
                formatter.Serialize(fs, obj);
            return filePath;
        }

        public static Model Deserialize(string filePath)
        {
            var formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
                return (Model)formatter.Deserialize(fs);
        }
    }
}
