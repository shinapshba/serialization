﻿using System;

namespace Serializer
{
    [Serializable]
    struct Plane
    {
        string[] values;

        public Plane(string[] values)
        {
            this.values = values;
        }

        public override string ToString()
        {
            var result = "f ";
            foreach (string val in values)
                result += val + " ";
            return result.Trim();
        }
    }

    [Serializable]
    struct Point
    {
        double X;
        double Y;
        double Z;

        public Point(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public override string ToString()
        {
            return $"v {X.ToString().Replace(",", ".")} {Y.ToString().Replace(",", ".")} {Z.ToString().Replace(",", ".")}";
        }
    }

    [Serializable]
    struct Vector
    {
        double X;
        double Y;

        public Vector(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return $"vt {X.ToString().Replace(",", ".")} {Y.ToString().Replace(",", ".")}";
        }
    }

    [Serializable]
    struct Normale
    {
        double X;
        double Y;
        double Z;

        public Normale(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public override string ToString()
        {
            return $"vn {X.ToString().Replace(",", ".")} {Y.ToString().Replace(",", ".")} {Z.ToString().Replace(",", ".")}";
        }
    }
}
